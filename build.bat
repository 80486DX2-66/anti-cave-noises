@echo off
set "input_dir=src"
set "output_dir=build"
set "mcrespath=%MINECRAFT_PATH%\resourcepacks"
set "mutual_snd_path=assets\minecraft\sounds\ambient\cave"

rem main branch
set "mcversion="

if "%1" equ "/?" goto :help
if "%1" equ "-h" goto :help
if "%1" equ "--help" goto :help

if "%1" equ "build" (
	if "%2" equ "" (
		call :display_available_versions
		set /p "mcversion=Target Minecraft version: "
	) else (
		set "mcversion=%2"
	)
	goto :prepare
) else if "%1" equ "clean" (
	rd /S /Q ".\build"
	echo Build directory cleaned.
	goto :EOF
) else (
	goto :help
)

:build
set "packname=anti-cave-noises-%mcversion%.zip"
set "pm_out=%output_dir%\pack.mcmeta"
set "snd_src=%input_dir%\%mutual_snd_path%\"
set "snd_out=%output_dir%\%mutual_snd_path%\"

copy "%input_dir%\pack_%mcversion%.mcmeta" "%pm_out%"
copy "%input_dir%\pack.png" "%output_dir%\pack.png"
call :amkdir %snd_out%

set cave_sounds_limit=13
if "%mcversion%" equ "1.9" do (
	set cave_sounds_limit=14
) else if "%mcversion%" equ "1.10" do (
	set cave_sounds_limit=16
) else if "%mcversion%" equ "1.12" do (
	set cave_sounds_limit=18
)
set 1_13_x_plus=0
if "%mcversion%" equ "1.13" set 1_13_x_plus=1
if "%mcversion%" equ "1.14" set 1_13_x_plus=1
if "%mcversion%" equ "1.15" set 1_13_x_plus=1
if "%mcversion%" equ "1.16" set 1_13_x_plus=1
if "%mcversion%" equ "1.17" set 1_13_x_plus=1
if "%mcversion%" equ "1.18" set 1_13_x_plus=1
if "%mcversion%" equ "1.19" set 1_13_x_plus=1
if "%mcversion%" equ "1.20" set 1_13_x_plus=1
if "%1_13_x_plus%" equ "1" set cave_sounds_limit=19

for /L %%i in (1, 1, %cave_sounds_limit%) do (
	set cur_file="%snd_src%\cave%%i.ogg"
	if exist %cur_file% (
		copy %cur_file% "%snd_out%"
		set last_existent_snd=%cur_file%
	) else if defined %last_existent_snd% do (
		copy %last_existent_snd% "%snd_out%"
	)
	if "%errorlevel%" gtr 0 exit 2
)
copy "%snd_src%\cave_%mcversion%.json" "%snd_out%\cave.json"

rem xcopy /Y /E "%input_dir%\assets" "%output_dir%\assets"

:compile
cd "%output_dir%"
7z.exe a -r -x!%0 -mx0 "..\%packname%" "*"
cd ..
move /-Y "%packname%" "%mcrespath%\%packname%"
pause
goto :EOF

:clean
call :adel "%output_dir%\*.*"
call :adel "%output_dir%\%mutual_snd_path%\*.*"
call :ard "%output_dir%\%mutual_snd_path%"
exit /b

:help
echo Usage: %~nx0 ^<command^> ^<target Minecraft version^>
echo Commands:
echo:/^?,
echo -h,
echo --help : Displays this help message
echo build  : Builds the resource pack
echo clean  : Cleans the build directory
echo.
call :display_available_versions
goto :EOF

:display_available_versions
echo Available Minecraft versions to build for:
echo - 1.12
echo - 1.20
echo.
exit /b

:adel
if exist %1 ( del /Q %1 )
exit /b

:ard
if exist %1 ( rd /Q %1 )
exit /b

:amkdir
if not exist %1 ( mkdir %1 )
exit /b

:EOF
