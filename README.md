# Anti-Cave Noises Resource Pack

## Table of Contents
1. [Description](#description)
2. [Building and Installing](#building-and-installing)
   1. [*nix systems](#nix-systems)
   2. [MS Windows](#ms-windows)
3. [Legal Notice](#legal-notice)

## Description
A Minecraft audio-only resource pack crafted to replace the ambient cave sounds.

## Building and Installing
Requirements for building:
- [7-Zip](https://www.7-zip.org) or [package `p7zip`](https://sourceforge.net/projects/p7zip/) should be installed and available as a simple command `7z`.

Additional requirements for installing:
- Environmental variable `$MINECRAFT_PATH`/`%MINECRAFT_PATH%` to be set and pointing to the actual game path.

### *nix systems
Requirements:
- Bash or a shell that can process Bash-like scripts

```sh
# to build
./build.sh build [version]

# to install (after building)
./build.sh install [version]
```

### MS Windows
```batch
:: to build
.\build.bat build [version]

:: to install (after building)
.\build.bat install [version]
```

## Legal Notice
**Legal notice:** This free, open-source resource pack is an independent creation and is not affiliated with Minecraft or Microsoft. Any references to Minecraft or Microsoft are for descriptive purposes only. Use of this resource pack is subject to the terms and conditions outlined in the accompanying license.
