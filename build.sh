#!/bin/bash

input_dir="./src"
output_dir="./build"
mcrespath="$MINECRAFT_PATH/resourcepacks"
packname_prefix="anti-cave-noises-"
packname_ext="zip"
mutual_snd_path="assets/minecraft/sounds/ambient/cave"

pause() {
	read -n 1 -sp "Press any key to continue..."
	echo
}

build() {
	packname="$packname_prefix$mcversion.$packname_ext"
	amkdir "$output_dir"

	pm_out="$output_dir/pack.mcmeta"
	snd_src="$input_dir/$mutual_snd_path"
	snd_out="$output_dir/$mutual_snd_path"

	amkdir "$snd_out/"
	cp "$input_dir/pack_$mcversion.mcmeta" "$pm_out"
	cp "$input_dir/pack.png" "$output_dir/pack.png"

	cave_sounds_limit=13
	if [ "$mcversion" = "1.9" ]; then
		cave_sounds_limit=14
	elif [ "$mcversion" = "1.10" ] || [ "$mcversion" = "1.11" ]; then
		cave_sounds_limit=16
	elif [ "$mcversion" = "1.12" ]; then
		cave_sounds_limit=18
	elif [ "$mcversion" = "1.13" ] || [ "$mcversion" = "1.14" ] ||
		[ "$mcversion" = "1.15" ] || [ "$mcversion" = "1.15" ] ||
		[ "$mcversion" = "1.16" ] || [ "$mcversion" = "1.17" ] ||
		[ "$mcversion" = "1.18" ] || [ "$mcversion" = "1.19" ] ||
		[ "$mcversion" = "1.20" ]; then
		cave_sounds_limit=19
	fi

	last_existent_snd=""

	for ((i = 1; i <= $cave_sounds_limit; i++)); do
		cur_file="$snd_src/cave$i.ogg"
		if [ -e "$cur_file" ]; then
			cp "$cur_file" "$snd_out"
			last_existent_snd="$cur_file"
		elif [ -e "$last_existent_snd" ]; then
			cp "$last_existent_snd" "$snd_out"
		fi
		if [ $? -gt 0 ]; then
			exit 2
		fi
	done

	cp "$snd_src/cave_$mcversion.json" "$snd_out/cave.json"

	compile
}

compile() {
	cd "$output_dir"
	7z a -r -x!$0 -mx0 "../$packname" "../$input_dir/*"
	cd ..
	mv -i "$packname" "$mcrespath/$packname"
	pause
}

clean() {
	adel "$output_dir/*.*"
	adel "$output_dir/$mutual_snd_path/*.*"
	adel "./$packname_prefix*.$packname_ext"
	ard "$output_dir/$mutual_snd_path"
}

display_help() {
	echo "Usage: $0 <command> <target Minecraft version>"
	echo "Commands:"
	echo "/?,"
	echo "-h,"
	echo "--help : Displays this help message"
	echo "build  : Builds the resource pack"
	echo "clean  : Cleans the build directory"
	echo
	display_available_versions
}

display_available_versions() {
	echo "Available Minecraft versions to build for:"
	echo "- 1.12"
	echo "- 1.20"
	echo
}

adel() {
	if [ -e "$1" ]; then
		rm -f "$1"
	fi
}

ard() {
	if [ -e "$1" ]; then
		rm -rf "$1"
	fi
}

amkdir() {
	if [ ! -d "$1" ]; then
		mkdir -p "$1"
	fi
}

# main branch
mcversion=""

if [ "$1" = "/?" ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
	display_help
	exit 0
fi

if [ "$1" = "build" ]; then
	if [ -z "$2" ]; then
		display_available_versions
		read -p "Target Minecraft version: " mcversion
	else
		mcversion="$2"
	fi
	clean
	build
elif [ "$1" = "clean" ]; then
	clean
	rm -rf "$output_dir"
	echo "Build directory cleaned."
else
	display_help
	exit 1
fi
exit 0
